// SECTION - MongoDB Aggregation
/* 
    - used to generate manipulated data and perform operations to create filtered results that help in analyzing data
    - compared to CRUD operations, aggregations gives us access to manipulate, filter, and compute for results providing us with information to make necessary development decisions without having to create a frontend application
*/

// $match
/* 
    - used to pass the documents that meet the specified condition(s) to the next pipeline/aggregation process
    - pipelines/aggregation process is the series of aggregation methods should the dev/client wants to use two or more aggregation methods in one statement. MongoDB will treat this as stages wherein it will not proceed to the next pipeline, unless it is done with the first
    SYNTAX:
        { $match: { field : value } }
*/

db.fruits.aggregate([
    { $match: { onSale: true } }
]);

// $group
/* 
    - used to group elements together and field-value pairs using the data from the grouped elements
    SYNTAX:
        { $group: { _id: "$value", fieldResult: "$valueResult" } }
*/
db.fruits.aggregate([
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
]);

// using both $match and $group along with aggregation will find for the products that are onSale and will group the total stocks for all suppliers found
/* 
    SYNTAX:
        db.collectionName.aggregate([
            { $match: { onSale: true } },
            { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
        ]);
*/
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
]);

// Field projections with aggregation

// $project
/* 
    - can be used when aggregating data to include/exclude fields from the returned result
    SYNTAX:
        { $project: { field: 1/0 } }
*/
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
    { $project: { _id: 0 } }
]);

// $sort
/* 
    - used to change the order of aggregated results
    - providing -1 as the value will result to MongoDB sorting the documents in reversed order
    SYNTAX:
        { $sort: { field: 1/-1 } }
*/
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
    { $sort: { total: -1 } }
]);

db.fruits.aggregate([
    { $group : { _id : "$origin", kinds: { $sum : 1 } } }
]);

// $unwind
/* 
    - used to deconstruct an array field from a collection/field with array value to output a result for each element.
    - this will result to creating separate documents for each of the elements inside the array
    SYNTAX:
        { $unwind: field }
*/
// Using the code below, we are instructing MongoDB to deconstruct first the documents based on the origin field and group them based on the number of times a country is mentioned in the "origin" field
db.fruits.aggregate([
    { $unwind : "$origin" },
    { $group : { _id : "$origin", kinds: { $sum : 1 } } }
]);

// SECTION - Schema Design
/* 
    - schema design/data modeling is an important feature when creating databases
    - MongoDB documents can be categorized into normalized or de-normalized/embedded data
    - Normalized data refers to a data structure where documents are referred to each other using their ids for related pieces of information
    - De-normalized data/embedded data design refers to a data structure where related pieces of information are addded to a document as an embedded object
*/
var owner = ObjectId();

db.owners.insert({
    _id: owner,
    name: "John Smith",
    contact: "09123456789"
});

db.suppliers.insert({
    name: "John Smith",
    contact: "09123456789",
    owner_id: ObjectId("62d54765e49fbbee6a1f4627")
});

// de-normalized data schema design with one-to-few relationship
db.suppliers.insert({
    name: "DEF fruits",
    contact: "09123456789",
    address: [
        {
            street: "123 San Jose St.",
            city: "Manila"
        },
        {
            street: "367 Gil Puyat",
            city: "Makita"
        }
    ]
});

/*  
    - Both data structures are common practices but each of them has its own pros and cons
    Difference between Normalized and De-normalized

    NORMALIZED
        - it makes it easier to read information because separate documents can be retrieved.
        - in terms of querying results, it performs slower compared to embedded data due to having to retrieve multiple documents at the same time.
        - this 

    DE-NORMALIZED
        - it makes it easier to query documents and has a faster performance because only one query needs to be done in order to retrieve the document/s.
        - if the data structure becomes too complex and long it makes it more difficult to manipulate and access information
        - this approach is applicable for data structures where pieces of information are commonly retrieved and rarely operated on/changed
*/

let supplier = ObjectId();
let branch = ObjectId();

db.suppliers.insert({
    _id: supplier,
    name: "Farm Fresh Co.",
    contact: "09987654321",
    branches: [branch]
});

db.branch.insert({
    _id: branch,
    name: "Farm Fresh Co. Cebu",
    address: "33 Arellano Ave, Bulan",
    city: "Cebu City",
    supplier_id: supplier
});